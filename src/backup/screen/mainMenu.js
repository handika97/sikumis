import React, {useEffect, useContext, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  NativeModules,
  ImageBackground,
  TextInput,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ScrollView} from 'react-native-gesture-handler';
import axios from 'axios';
import {authContext} from '../context/authSlice';
import {listenerContext} from '../context/listener';
import RNCalendarEvents from 'react-native-calendar-events';
import PushNotification from 'react-native-push-notification';

const mainMenu = ({navigation}) => {
  const {auth, dispatch_auth} = useContext(authContext);
  const {list, dispatch_listener} = useContext(listenerContext);

  const [CurahHujan, setCurahHujan] = useState('');
  const [Kelembapan, setKelembapan] = useState('');
  // const [Indikator, setIndikator] = useState('');
  const [Fuzzy, setFuzzy] = useState(0);
  const [Sampel, setSampel] = useState(90);
  const [Tanggal, setTanggal] = useState('');
  const [mode, setmode] = useState(true);

  useEffect(() => {
    if (list.now && list.fuzzy) {
      if (
        new Date(`${list.now}`).getDate() + list.fuzzy - new Date().getDate() <=
        0
      ) {
        navigation.navigate('Siram');
      }
    }
  }, []);

  const getData = () => {
    const url = `https://platform.antares.id:8443/~/antares-cse/antares-id/sikumis/arduino/la`;
    axios
      .get(url, {
        headers: {
          'X-M2M-Origin': '0ee9462898298171:102854b81d204af5',
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
      })
      .then((res) => {
        let nilai = JSON.parse(res.data['m2m:cin'].con);
        console.log(nilai);
        // setCurahHujan(nilai.CurahHujan);
        // setKelembapan(nilai.Kelembapan);
        dispatch_listener({
          type: 'GET',
          Kelembapan: parseFloat(nilai.Kelembapan),
          Curah_Hujan: parseFloat(nilai.CurahHujan),
        });
      })
      .catch((e) => {
        console.log(e);
        alert(e);
      });
  };

  const hitung = () => {
    if (mode) {
      if (parseFloat(list.Kelembapan) && parseFloat(list.Curah_Hujan) >= 0) {
        let kelembapan = parseFloat(list.Kelembapan);
        let curah_ujan = parseFloat(list.Curah_Hujan);
        Hitung(kelembapan, curah_ujan);
      } else {
        alert('Klik Tombol Data');
      }
    } else {
      if (Kelembapan && CurahHujan) {
        let kelembapan = parseFloat(Kelembapan);
        let curah_ujan = parseFloat(CurahHujan);
        Hitung(kelembapan, curah_ujan);
      } else {
        alert('Isi Kelembapan dan Curah Hujan');
      }
    }
  };

  const Hitung = () => {
    let kelembapan = 0;
    let curah_ujan = 0;
    console.log(CurahHujan, Kelembapan);
    if (mode) {
      kelembapan = parseFloat(list.Kelembapan);
      curah_ujan = parseFloat(list.Curah_Hujan);
    } else {
      kelembapan = parseFloat(Kelembapan);
      curah_ujan = parseFloat(CurahHujan);
    }
    let kelembapan_K = 0;
    let kelembapan_L = 0;
    let kelembapan_B = 0;
    let curah_ujan_R = 0;
    let curah_ujan_S = 0;
    let curah_ujan_T = 0;
    if (kelembapan <= 30) {
      kelembapan_K = 1;
    } else if (kelembapan > 30 && kelembapan < 40) {
      kelembapan_K = (40 - kelembapan) / 10;
    } else {
      kelembapan_K = 0;
    }

    if (kelembapan >= 40 && kelembapan <= 70) {
      kelembapan_L = 1;
    } else if (kelembapan > 30 && kelembapan < 40) {
      kelembapan_L = (kelembapan - 30) / 10;
    } else if (kelembapan > 70 && kelembapan < 80) {
      kelembapan_L = (80 - kelembapan) / 10;
    } else {
      kelembapan_L = 0;
    }

    if (kelembapan >= 80) {
      kelembapan_B = 1;
    } else if (kelembapan > 70 && kelembapan < 80) {
      kelembapan_B = (kelembapan - 70) / 10;
    } else {
      kelembapan_B = 0;
    }

    if (curah_ujan <= 1) {
      curah_ujan_R = 1;
    } else if (curah_ujan > 1 && curah_ujan < 3) {
      curah_ujan_R = (3 - curah_ujan) / 2;
    } else {
      curah_ujan_R = 0;
    }

    if (curah_ujan >= 3 && curah_ujan <= 4) {
      curah_ujan_S = 1;
    } else if (curah_ujan > 1 && curah_ujan < 3) {
      curah_ujan_S = (curah_ujan - 1) / 2;
    } else if (curah_ujan > 4 && curah_ujan < 5) {
      curah_ujan_S = (5 - curah_ujan) / 1;
    } else {
      curah_ujan_S = 0;
    }

    if (curah_ujan >= 5) {
      curah_ujan_T = 1;
    } else if (curah_ujan > 4 && curah_ujan < 5) {
      curah_ujan_T = (curah_ujan - 4) / 1;
    } else {
      curah_ujan_T = 0;
    }
    console.log('kelembapan', kelembapan_K, kelembapan_L, kelembapan_B);
    console.log('curah ujan', curah_ujan_R, curah_ujan_S, curah_ujan_T);

    let dekat = Math.max(
      Math.min(kelembapan_B, curah_ujan_R),
      Math.min(kelembapan_L, curah_ujan_S),
      Math.min(kelembapan_B, curah_ujan_S),
      Math.min(kelembapan_K, curah_ujan_T),
      Math.min(kelembapan_L, curah_ujan_T),
      Math.min(kelembapan_B, curah_ujan_T),
    );

    let normal = Math.max(
      Math.min(kelembapan_L, curah_ujan_R),
      Math.min(kelembapan_K, curah_ujan_S),
    );

    let lama = Math.min(kelembapan_K, curah_ujan_R);

    let delta = 90 / Sampel;
    let titik_sampel = delta;
    let hasilPembilang = 0;
    let jumlah_sampelAB = 0;
    let jumlah_sampelCD = 0;
    let jumlah_sampelEF = 0;
    let jumlah_sampelBC = 0;
    let jumlah_sampelDE = 0;
    for (let i = 1; i <= Sampel; i++) {
      if (titik_sampel <= 7) {
        hasilPembilang += titik_sampel * dekat;
        jumlah_sampelAB += 1;
      } else if (titik_sampel >= 14 && titik_sampel <= 28) {
        hasilPembilang += titik_sampel * normal;
        jumlah_sampelCD += 1;
      } else if (titik_sampel >= 84) {
        hasilPembilang += titik_sampel * lama;
        jumlah_sampelEF += 1;

        // } else if (titik_sampel > 7 && titik_sampel < 14 && (dekat || normal)) {
        //   if (dekat > normal) {
        //     let sampelBC = (14 - titik_sampel) / 7;
        //     jumlah_sampelBC += (14 - titik_sampel) / 7;
        //     hasilPembilang += titik_sampel * sampelBC;
        //   } else {
        //     let sampelBC = (titik_sampel - 7) / 7;
        //     console.log('lama', sampelBC);
        //     jumlah_sampelBC += (titik_sampel - 7) / 7;
        //     hasilPembilang += titik_sampel * sampelBC;
        //   }
        // } else if (titik_sampel > 28 && titik_sampel < 84 && (lama || normal)) {
        //   if (normal > lama) {
        //     let sampelDE = (84 - titik_sampel) / 56;
        //     console.log('normal', sampelDE);
        //     jumlah_sampelDE += (84 - titik_sampel) / 56;
        //     hasilPembilang += titik_sampel * sampelDE;
        //   } else {
        //     console.log('lama');
        //     let sampelDE = (titik_sampel - 28) / 56;
        //     jumlah_sampelDE += (titik_sampel - 28) / 56;
        //     hasilPembilang += titik_sampel * sampelDE;
        //   }
      }
      console.log(titik_sampel, delta);
      titik_sampel += delta;
    }
    let hasilPenyebut =
      jumlah_sampelAB * dekat +
      jumlah_sampelCD * normal +
      jumlah_sampelEF * lama +
      jumlah_sampelDE +
      jumlah_sampelBC;
    console.log(
      hasilPenyebut,
      hasilPembilang,
      jumlah_sampelAB * dekat,
      jumlah_sampelCD * normal,
      jumlah_sampelEF * lama,
      jumlah_sampelDE,
      jumlah_sampelBC,
    );
    let hasilFuzzy = hasilPembilang / hasilPenyebut;
    // (dekat * 28 + normal * 168 + lama * 609) /
    // (dekat * 7 + normal * 8 + lama * 7);
    let Indikator = '';
    if (hasilFuzzy <= 7) {
      Indikator = 'Dekat';
    } else if (hasilFuzzy <= 28) {
      Indikator = 'Normal';
    } else {
      Indikator = 'Lama';
    }
    setFuzzy(parseInt(hasilFuzzy));
    console.log(
      'dekat ',
      dekat,
      ' normal ',
      normal,
      ' lama ',
      lama,
      'hasilFuzzy',
      hasilFuzzy,
    );
    PushNotification.localNotificationSchedule({
      /* iOS and Android properties */
      title: 'Siram Tanaman', // (optional)
      message: 'Jangan Lupa Siram Tanaman Anda',
      // vibrate: true, // (optional) default: true
      // vibration: 300,
      date: new Date(Date.now() + hasilFuzzy * 86400000),
    });
    // setTanggal(
    //   `${
    //     new Date(Date.now() + hasilFuzzy * 86400000).getDate() +
    //     '/' +
    //     (new Date(Date.now() + hasilFuzzy * 86400000).getMonth() + 1) +
    //     '/' +
    //     new Date(Date.now() + hasilFuzzy * 86400000).getFullYear()
    //   }`,
    // );
    dispatch_listener({
      type: 'ON',
      date: `${
        new Date(Date.now() + hasilFuzzy * 86400000).getDate() +
        '/' +
        (new Date(Date.now() + hasilFuzzy * 86400000).getMonth() + 1) +
        '/' +
        new Date(Date.now() + hasilFuzzy * 86400000).getFullYear()
      }`,
      fuzzy: parseInt(hasilFuzzy),
      now: new Date(),
      Indikator: Indikator,
      // Kelembapan: parseFloat(nilai.Kelembapan),
      // Curah_Hujan: nilai.CurahHujan,
    });
  };

  return (
    <View style={{flex: 1, backgroundColor: '#e8cecd'}}>
      <View
        style={{
          paddingVertical: hp(4),
          paddingHorizontal: wp(2),
        }}>
        <View
          style={{
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
          }}>
          <TouchableOpacity
            onPress={() => (mode ? setmode(false) : setmode(true))}
            style={{
              height: hp(9),
              width: hp(9),
            }}>
            <Image
              source={require('../asset/logo.png')}
              style={{height: '100%', width: '100%'}}
            />
          </TouchableOpacity>
        </View>
        {!mode ? (
          <>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                backgroundColor: 'white',
                width: wp(80),
                alignSelf: 'center',
                marginTop: hp(7),
                height: hp(8),
                borderRadius: 20,
                borderWidth: 1,
                paddingHorizontal: wp(1),
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View style={{height: hp(5), width: hp(5), marginRight: wp(2)}}>
                  <Image
                    source={require('../asset/ph.png')}
                    style={{height: '100%', width: '100%'}}
                  />
                </View>
                <Text style={{fontSize: hp(2.4)}}>Kelembapan</Text>
              </View>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <TextInput
                  style={{borderWidth: 1.5, color: 'red', fontSize: hp(3.5)}}
                  keyboardType="decimal-pad"
                  value={Kelembapan}
                  onChangeText={setKelembapan}
                />
                <View style={{marginLeft: wp(4), width: wp(7)}}>
                  <Text style={{fontSize: hp(2)}}>%</Text>
                </View>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                backgroundColor: 'white',
                width: wp(80),
                alignSelf: 'center',
                marginTop: hp(3),
                height: hp(8),
                borderRadius: 20,
                borderWidth: 1,
                paddingHorizontal: wp(1),
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View style={{height: hp(5), width: hp(5), marginRight: wp(2)}}>
                  <Image
                    source={require('../asset/hujan.png')}
                    style={{height: '100%', width: '100%'}}
                  />
                </View>
                <Text style={{fontSize: hp(2.4)}}>Curah Hujan</Text>
              </View>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <TextInput
                  style={{borderWidth: 1.5, color: 'red', fontSize: hp(3.5)}}
                  keyboardType="decimal-pad"
                  value={CurahHujan}
                  onChangeText={setCurahHujan}
                />
                <View style={{marginLeft: wp(4), width: wp(7)}}>
                  <Text style={{fontSize: hp(2)}}>mm</Text>
                </View>
              </View>
            </View>
          </>
        ) : (
          <>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                backgroundColor: 'white',
                width: wp(80),
                alignSelf: 'center',
                marginTop: hp(7),
                height: hp(8),
                borderRadius: 20,
                borderWidth: 1,
                paddingHorizontal: wp(1),
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View style={{height: hp(5), width: hp(5), marginRight: wp(2)}}>
                  <Image
                    source={require('../asset/ph.png')}
                    style={{height: '100%', width: '100%'}}
                  />
                </View>
                <Text style={{fontSize: hp(2.4)}}>Kelembapan</Text>
              </View>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text style={{color: 'red', fontSize: hp(3.5)}}>
                  {list.Kelembapan}
                </Text>
                <View style={{marginLeft: wp(4), width: wp(7)}}>
                  <Text style={{fontSize: hp(2)}}>%</Text>
                </View>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                backgroundColor: 'white',
                width: wp(80),
                alignSelf: 'center',
                marginTop: hp(3),
                height: hp(8),
                borderRadius: 20,
                borderWidth: 1,
                paddingHorizontal: wp(1),
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View style={{height: hp(5), width: hp(5), marginRight: wp(2)}}>
                  <Image
                    source={require('../asset/hujan.png')}
                    style={{height: '100%', width: '100%'}}
                  />
                </View>
                <Text style={{fontSize: hp(2.4)}}>Curah Hujan</Text>
              </View>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text style={{color: 'red', fontSize: hp(3.5)}}>
                  {`${list.Curah_Hujan}`}
                </Text>
                <View style={{marginLeft: wp(4), width: wp(7)}}>
                  <Text style={{fontSize: hp(2)}}>mm</Text>
                </View>
              </View>
            </View>
          </>
        )}
        <TouchableOpacity onPress={() => getData()}>
          <View
            style={{
              alignSelf: 'center',
              marginTop: hp(5),
              backgroundColor: '#d4d8d7',
              height: hp(6.5),
              width: wp(40),
              alignItems: 'center',
              justifyContent: 'center',
              borderWidth: 0.5,
              elevation: 5,
            }}>
            <Text style={{fontSize: hp(4)}}>DATA</Text>
          </View>
        </TouchableOpacity>
        <View
          style={{
            alignSelf: 'center',
            marginTop: hp(3),
            borderBottomWidth: 2,
            width: wp(40),
            alignItems: 'center',
            borderBottomColor: '#276fa0',
          }}>
          <Text style={{fontSize: hp(3.7), color: '#3372aa'}}>Indikator</Text>
          <Text style={{fontSize: hp(3.2), color: 'black'}}>
            {list.Indikator}
          </Text>
        </View>
        <View
          style={{
            justifyContent: 'center',
            width: wp(60),
            alignSelf: 'center',
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <View>
              {console.log(list.status)}
              {list.status && (
                <TouchableOpacity
                  onPress={() => {
                    // setCurahHujan(0),
                    //   setFuzzy(0),
                    //   // setIndikator(''),
                    //   setKelembapan(0);
                    PushNotification.cancelAllLocalNotifications();
                    dispatch_listener({
                      type: 'OFF',
                    });
                  }}
                  style={{
                    alignSelf: 'center',
                    marginTop: hp(3),
                    backgroundColor: '#d4d8d7',
                    height: hp(6.5),
                    width: wp(25),
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderWidth: 0.5,
                    elevation: 5,
                  }}>
                  <Text style={{fontSize: hp(3.5), fontWight: 'bold'}}>
                    RESET
                  </Text>
                </TouchableOpacity>
              )}
            </View>
            <View>
              {!list.status && (
                <TouchableOpacity
                  onPress={() => hitung()}
                  style={{
                    alignSelf: 'center',
                    marginTop: hp(3),
                    backgroundColor: '#d4d8d7',
                    height: hp(6.5),
                    width: wp(25),
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderWidth: 0.5,
                    elevation: 5,
                  }}>
                  <Text style={{fontSize: hp(3.3), fontWight: 'bold'}}>
                    HITUNG
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </View>
        <View
          style={{
            alignSelf: 'center',
            marginTop: hp(5),
            justifyContent: 'center',
            alignItems: 'center',
            borderBottomColor: 'black',
            borderBottomWidth: 1,
          }}>
          <Text style={{fontSize: hp(3.5), fontWight: 'bold', color: 'red'}}>
            Sisa Waktu Siram Girkulan:
          </Text>
          <Text
            style={{
              fontSize: hp(3),
              fontWight: 'bold',
              color: 'red',
            }}>
            {list.date ? (
              <>
                {new Date(`${list.now}`).getDate() +
                  list.fuzzy -
                  new Date().getDate()}{' '}
                Hari({list.date})
              </>
            ) : (
              <>0 Hari</>
            )}
          </Text>
        </View>
      </View>
      <View
        style={{
          position: 'absolute',
          bottom: 0,
          backgroundColor: '#92c4e7',
          width: '100%',
          height: hp(8),
          justifyContent: 'space-between',
          alignItems: 'flex-end',
          flexDirection: 'row',
        }}>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPress={() =>
            dispatch_auth({
              type: 'LOG_OFF',
            })
          }>
          <View
            style={{
              height: hp(7),
              width: hp(7),
              marginLeft: wp(2),
            }}>
            <Image
              source={require('../asset/user.png')}
              style={{height: '100%', width: '100%'}}
            />
          </View>
          <Text>Keluar</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPress={() => navigation.navigate('Siram')}>
          <Text>Selanjutnya</Text>
          <View style={{height: hp(7), width: hp(7), marginLeft: wp(2)}}>
            <Image
              source={require('../asset/arrow2.png')}
              style={{height: '100%', width: '100%'}}
            />
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // flexDirection: 'column',
    // backgroundColor: 'white',
    // alignItems: 'center',
  },
});

export default mainMenu;
