import React, {useContext, useEffect, useState} from 'react';
import {Image, View, StatusBar, Button, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import mainMenu from '../screen/mainMenu';
import Siram from '../screen/Siram';
import homeLogin from '../screen/homeLogin';
import register from '../screen/register';
import login from '../screen/login';
import splash from '../screen/splash';
import intro from '../screen/intro';
import introText from '../screen/introText';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {authContext} from '../context/authSlice';

const StackApp = createStackNavigator();

const navOptionHandler = () => ({
  headerShown: false,
  tabBarOptions: {activeTintColor: '#F9BA9B'},
  animationEnabled: 'false',
});

export default function MainNavigator() {
  const {auth, dispatch_auth} = useContext(authContext);
  const [perform, setperform] = useState(true);
  useEffect(() => {
    setTimeout(() => {
      setperform(false);
    }, 2000);
  }, []);
  return (
    <NavigationContainer>
      <StackApp.Navigator>
        {perform && (
          <StackApp.Screen
            name="splash"
            component={splash}
            options={navOptionHandler}
          />
        )}
        {!auth.isLogin ? (
          <>
            <StackApp.Screen
              name="intro"
              component={intro}
              options={navOptionHandler}
            />
            <StackApp.Screen
              name="introText"
              component={introText}
              options={navOptionHandler}
            />
            <StackApp.Screen
              name="homeLogin"
              component={homeLogin}
              options={navOptionHandler}
            />
            <StackApp.Screen
              name="register"
              component={register}
              options={navOptionHandler}
            />
            <StackApp.Screen
              name="login"
              component={login}
              options={navOptionHandler}
            />
          </>
        ) : (
          <>
            <StackApp.Screen
              name="mainMenu"
              component={mainMenu}
              options={navOptionHandler}
            />
            <StackApp.Screen
              name="Siram"
              component={Siram}
              options={navOptionHandler}
            />
          </>
        )}
      </StackApp.Navigator>
    </NavigationContainer>
  );
}
