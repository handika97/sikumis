const initialState = {
  status: false,
  values: {},
  value: '',
};

export const listenerReducer = (state, action) => {
  switch (action.type) {
    case 'GET':
      return {
        ...state,
        Kelembapan: action.Kelembapan,
        Curah_Hujan: action.Curah_Hujan,
      };
    case 'ON':
      return {
        ...state,
        status: true,
        date: action.date,
        fuzzy: action.fuzzy,
        now: action.now,
        Indikator: action.Indikator,
      };
    case 'OFF':
      return {
        ...state,
        status: false,
        date: '',
        fuzzy: '',
        now: '',
        Indikator: '',
        Curah_Hujan: '',
        Kelembapan: '',
      };
    case 'VALUES':
      return {
        values: action.values,
      };
    case 'VALUE':
      return {
        ...state,
        value: action.value,
      };
    case 'RESET_lANG': {
      console.log('action', action.current_lang);
      return action.current_lang;
    }
    default:
      return state;
  }
};
