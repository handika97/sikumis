import React, {useEffect, useContext, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  NativeModules,
  ImageBackground,
  TextInput,
  ActivityIndicator,
  Modal,
} from 'react-native';
import database from '@react-native-firebase/database';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ScrollView} from 'react-native-gesture-handler';
import {authContext} from '../context/authSlice';
import authh from '@react-native-firebase/auth';

const Login = ({navigation}) => {
  const {auth, dispatch_auth} = useContext(authContext);
  const [Username, setUsername] = useState('');
  const [Password, setPassword] = useState('');
  const [Visible, setVisible] = useState(false);
  const [isVisible, setisVisible] = useState(false);
  const [data, setdata] = useState('');
  console.log(auth);

  const login = () => {
    if (Username && Password) {
      setVisible(true);
      authh()
        .signInWithEmailAndPassword(Username, Password)
        .then(() => {
          // alert('User account created & signed in!');
          dispatch_auth({
            type: 'LOGIN',
            name: Username.toUpperCase(),
          });
          setVisible(false);
        })
        .catch((error) => {
          // if (error.code === 'auth/email-already-in-use') {
          //   alert('That email address is already in use!');
          // }

          // if (error.code === 'auth/invalid-email') {
          alert(error.code);
          // }
          setVisible(false);

          console.error(error);
        });
    }
  };

  const forget = () => {
    if (Username) {
      setVisible(true);
      setisVisible(false);
      authh()
        .sendPasswordResetEmail(Username)
        .then(() => {
          alert('Periksa Email Anda');
          // dispatch_auth({
          //   type: 'LOGIN',
          //   name: Username.toUpperCase(),
          // });
          setVisible(false);
        })
        .catch((error) => {
          // if (error.code === 'auth/email-already-in-use') {
          //   alert('That email address is already in use!');
          // }

          // if (error.code === 'auth/invalid-email') {
          alert(error.code);
          // }
          setVisible(false);

          console.error(error);
        });
    } else {
      alert('Username Tidak Boleh Kosong');
    }
    // auth.data.some;
  };

  return (
    <View style={styles.container}>
      {!Visible ? (
        <>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <View style={{height: wp(12), width: wp(80)}}>
              <Image
                source={require('../asset/sikumis1.png')}
                style={{height: '100%', width: '100%'}}
              />
            </View>
            <View
              style={{
                height: 50,
                width: wp(80),
                marginTop: hp(3),
                borderWidth: 1,
                borderRadius: 30,
                borderColor: 'black',
                alignItems: 'center',
                padding: 5,
                backgroundColor: 'white',
                flexDirection: 'row',
              }}>
              <View
                style={{
                  height: 35,
                  width: 35,
                  position: 'absolute',
                  marginLeft: wp('2'),
                }}>
                <Image
                  source={require('../asset/user.png')}
                  style={{height: '100%', width: '100%'}}
                />
              </View>
              <View style={{width: '100%', alignItems: 'center'}}>
                <TextInput
                  placeholder="Username"
                  value={Username}
                  onChangeText={setUsername}
                />
              </View>
            </View>
            <View
              style={{
                height: 50,
                width: wp(80),
                marginTop: hp(3),
                borderWidth: 1,
                borderRadius: 30,
                borderColor: 'black',
                alignItems: 'center',
                padding: 5,
                flexDirection: 'row',
                backgroundColor: 'white',
              }}>
              <View
                style={{
                  height: 35,
                  width: 35,
                  position: 'absolute',
                  marginLeft: wp('2'),
                }}>
                <Image
                  source={require('../asset/password.png')}
                  style={{height: '100%', width: '100%'}}
                />
              </View>
              <View style={{width: '100%', alignItems: 'center'}}>
                <TextInput
                  placeholder="Password"
                  secureTextEntry
                  value={Password}
                  onChangeText={setPassword}
                />
              </View>
            </View>
            <TouchableOpacity
              onPress={() => login()}
              style={{
                alignSelf: 'center',
                backgroundColor: '#d4d8d7',
                height: 43,
                width: wp(50),
                marginTop: hp(5),
                alignItems: 'center',
                justifyContent: 'center',
                borderWidth: 0.5,
                elevation: 5,
              }}>
              <Text
                style={{fontSize: hp(3.5), fontWight: 'bold', color: 'black'}}>
                Masuk
              </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={{
              flex: 0.4,
              alignItems: 'flex-end',
              padding: wp(3),
              borderRadius: 30,
              marginVertical: hp(1),
              position: 'absolute',
              bottom: 0,
              alignSelf: 'center',
            }}
            onPress={() => {
              setisVisible(true), setPassword('');
            }}>
            <Text style={{color: 'black'}}>Lupa Sandi?</Text>
          </TouchableOpacity>
        </>
      ) : (
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator size="large" color="black" />
        </View>
      )}
      <Modal
        animationType="slide"
        transparent={true}
        visible={isVisible}
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          // padding: 20,
        }}>
        <View style={{alignItems: 'center', justifyContent: 'center'}}>
          <View
            style={{
              height: hp(70),
              width: wp(90),
              marginTop: hp(5),
              backgroundColor: '#e8cecd',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View>
              <Text style={{fontSize: 20, color: 'black', fontWeight: 'bold'}}>
                Tulis Username Anda
              </Text>
            </View>
            <View
              style={{
                height: 50,
                width: wp(70),
                marginTop: hp(3),
                backgroundColor: 'white',
                borderWidth: 1,
                borderRadius: 30,
                borderColor: 'black',
                alignItems: 'center',
                padding: 5,
                flexDirection: 'row',
              }}>
              <View
                style={{
                  height: 35,
                  width: 35,
                  position: 'absolute',
                  marginLeft: wp('2'),
                }}>
                <Image
                  source={require('../asset/user.png')}
                  style={{height: '100%', width: '100%'}}
                />
              </View>
              <View style={{width: '100%', alignItems: 'center'}}>
                <TextInput
                  placeholder="Username"
                  value={Username}
                  onChangeText={setUsername}
                />
              </View>
            </View>

            <TouchableOpacity
              onPress={() => forget()}
              style={{
                alignSelf: 'center',
                backgroundColor: '#d4d8d7',
                height: 43,
                width: wp(50),
                marginTop: hp(5),
                alignItems: 'center',
                justifyContent: 'center',
                borderWidth: 0.5,
                elevation: 5,
              }}>
              <Text
                style={{fontSize: hp(3.5), fontWight: 'bold', color: 'black'}}>
                Ubah Sandi
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setisVisible(false)}
              style={{
                alignSelf: 'center',
                backgroundColor: '#d4d8d7',
                height: 43,
                width: wp(50),
                marginTop: hp(5),
                alignItems: 'center',
                justifyContent: 'center',
                borderWidth: 0.5,
                elevation: 5,
              }}>
              <Text
                style={{fontSize: hp(3.5), fontWight: 'bold', color: 'black'}}>
                Batal
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#e8cecd',
    justifyContent: 'center',
    paddingVertical: hp(23),
  },
  view: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    backgroundColor: 'gray',
    padding: 10,
    margin: 10,
  },
  text: {
    fontSize: 20,
    color: 'white',
  },
});

export default Login;
