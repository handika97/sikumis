import React, {useEffect, useContext, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  NativeModules,
  ImageBackground,
  TextInput,
} from 'react-native';
import database from '@react-native-firebase/database';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ScrollView} from 'react-native-gesture-handler';

const mainMenu = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <View style={{height: wp(12), width: wp(80), marginBottom: hp(5)}}>
          <Image
            source={require('../asset/sikumis1.png')}
            style={{height: '100%', width: '100%'}}
          />
        </View>
        <TouchableOpacity
          onPress={() => navigation.navigate('login')}
          style={{
            alignSelf: 'center',
            backgroundColor: '#d4d8d7',
            height: 43,
            width: wp(50),
            marginTop: hp(5),
            alignItems: 'center',
            justifyContent: 'center',
            borderWidth: 0.5,
            elevation: 5,
          }}>
          <Text style={{fontSize: hp(3.5), fontWight: 'bold', color: 'black'}}>
            Masuk
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('register')}
          style={{
            alignSelf: 'center',
            backgroundColor: '#d4d8d7',
            height: 43,
            width: wp(50),
            marginTop: hp(5),
            alignItems: 'center',
            justifyContent: 'center',
            borderWidth: 0.5,
            elevation: 5,
          }}>
          <Text style={{fontSize: hp(3.5), fontWight: 'bold', color: 'black'}}>
            Daftar
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#e8cecd',
    justifyContent: 'center',
  },
  view: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    backgroundColor: 'gray',
    padding: 10,
    margin: 10,
  },
  text: {
    fontSize: 20,
    color: 'white',
  },
});

export default mainMenu;
