import React, {createContext, useReducer, useEffect} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {authReducer} from '../reducers/auth';

export const authContext = createContext();

const AuthContextProvider = (props) => {
  const initialState = {
    isLogin: false,
    name: '',
    data: [],
  };

  const [auth, dispatch_auth] = useReducer(authReducer, initialState);

  const storeDataLang = async () => {
    try {
      console.log('ye');
      await AsyncStorage.setItem('auth', JSON.stringify(auth));
      //   AsyncStorage.clear();
    } catch (e) {
      // saving error
      console.log('error');
    }
  };

  const getDataLang = async () => {
    console.log('value');
    try {
      const value = await AsyncStorage.getItem('auth');
      if (value !== null) {
        dispatch_auth({
          type: 'RESET_LANG',
          current_lang: JSON.parse(value),
        });
      }
    } catch (e) {
      // error reading value
    }
  };

  useEffect(() => {
    getDataLang();
  }, []);

  useEffect(() => {
    storeDataLang();
    // console.log("test",lang)
  }, [auth]);

  return (
    <authContext.Provider value={{auth, dispatch_auth}}>
      {props.children}
    </authContext.Provider>
  );
};

export default AuthContextProvider;
