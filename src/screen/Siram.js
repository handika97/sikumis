import React, {useEffect, useContext, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  NativeModules,
  ImageBackground,
  Modal,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ScrollView} from 'react-native-gesture-handler';
import axios from 'axios';
import {Value} from 'react-native-reanimated';
import {listenerContext} from '../context/listener';

const mainMenu = ({route}) => {
  const {list, dispatch_listener} = useContext(listenerContext);
  const [Indikator, setIndikator] = useState(list.Indikator);
  const [Fuzzy, setFuzzy] = useState(0);
  const [value, setValue] = useState(0);
  const [siram, setsiram] = useState(false);
  const [Visible, setVisible] = useState(false);

  const Hitung = (nilai) => {
    const url = `https://platform.antares.id:8443/~/antares-cse/antares-id/sikumis/android`;
    axios
      .post(
        url,
        {
          'm2m:cin': {
            con: `{
            "penyiraman":${nilai}
            }`,
          },
        },
        {
          headers: {
            'X-M2M-Origin': '0ee9462898298171:102854b81d204af5',
            'Content-Type': 'application/json;ty=4',
            Accept: 'application/json',
          },
        },
      )
      .then((res) => {
        setsiram(true);
        alert('Berhasil Mengirim');
        dispatch_listener({
          type: 'OFF',
        });
      })
      .catch((e) => {
        console.log(e);
        alert('Gagal Mengirim');
      });
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#e8cecd',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <View style={{height: hp(30), width: hp(30)}}>
        <Image
          source={require('../asset/siram.gif')}
          style={{height: '100%', width: '100%'}}
        />
      </View>
      <View>
        <Text style={{fontSize: 25, marginTop: hp(1)}}>Siram Grikulan?</Text>
      </View>
      <View
        style={{
          justifyContent: 'center',
          width: wp(45),
          alignSelf: 'center',
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <TouchableOpacity
            onPress={() => {
              Hitung(1), setValue(1);
            }}
            style={{
              alignSelf: 'center',
              marginTop: hp(3),
              backgroundColor: '#d4d8d7',
              height: hp(6.5),
              width: wp(20),
              alignItems: 'center',
              justifyContent: 'center',
              borderWidth: 0.5,
              elevation: 5,
            }}>
            <Text style={{fontSize: hp(2.8), fontWight: 'bold'}}>YA</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              Hitung(0), setValue(0);
            }}
            style={{
              alignSelf: 'center',
              marginTop: hp(3),
              backgroundColor: '#d4d8d7',
              height: hp(6.5),
              width: wp(20),
              alignItems: 'center',
              justifyContent: 'center',
              borderWidth: 0.5,
              elevation: 5,
            }}>
            <Text style={{fontSize: hp(2.8), fontWight: 'bold'}}>TIDAK</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View
        style={{
          width: wp(100),
          alignSelf: 'center',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        {siram && value === 1 ? (
          <Text style={{fontSize: hp(3), marginTop: hp(5), color: 'red'}}>
            Penyiraman Grikulan Berhasil!
          </Text>
        ) : null}
      </View>
      <Modal
        animationType="slide"
        transparent={true}
        visible={Visible}
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          // padding: 20,
        }}>
        <View style={{alignItems: 'center', justifyContent: 'center'}}>
          <View
            style={{
              height: hp(70),
              width: wp(80),
              marginTop: hp(5),
              backgroundColor: 'white',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View style={{height: hp(25), width: hp(28)}}>
              <Image
                source={require('../asset/warning.gif')}
                style={{height: '100%', width: '100%'}}
              />
            </View>
            <View
              style={{
                alignSelf: 'center',
                marginTop: hp(3),
                borderBottomWidth: 2,
                width: wp(40),
                alignItems: 'center',
                borderBottomColor: '#276fa0',
              }}>
              <Text style={{fontSize: hp(3.7), color: '#3372aa'}}>
                Indikator
              </Text>
              <Text style={{fontSize: hp(3.2), color: 'black'}}>
                {Indikator}
              </Text>
            </View>
            <Text style={{fontSize: hp(2.2), color: '#3372aa'}}>
              Apakah Anda Yakin Tidak Menyiram Grikulan?
            </Text>
            <View
              style={{
                justifyContent: 'center',
                width: wp(45),
                alignSelf: 'center',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}>
                <TouchableOpacity
                  onPress={() => {
                    Hitung(0), setValue(0), setVisible(false);
                  }}
                  style={{
                    alignSelf: 'center',
                    marginTop: hp(3),
                    backgroundColor: '#d4d8d7',
                    height: hp(6.5),
                    width: wp(20),
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderWidth: 0.5,
                    elevation: 5,
                  }}>
                  <Text style={{fontSize: hp(3.5), fontWight: 'bold'}}>
                    YAKIN
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    setVisible(false);
                  }}
                  style={{
                    alignSelf: 'center',
                    marginTop: hp(3),
                    backgroundColor: '#d4d8d7',
                    height: hp(6.5),
                    width: wp(20),
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderWidth: 0.5,
                    elevation: 5,
                  }}>
                  <Text style={{fontSize: hp(3.5), fontWight: 'bold'}}>
                    BATAL
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default mainMenu;
