import 'react-native-gesture-handler';
{
  /* <script src="http://localhost:8097" />; */
}
import React from 'react';

// import {enableScreens} from 'react-native-screens';
// enableScreens();
import MainNavigator from './src/navigation/MainNavigation';

console.disableYellowBox = true;
import AuthContextProvider from './src/context/authSlice';
import ListenerContextProvider from './src/context/listener';

// store=createStore(qty)
export default function App() {
  return (
    <AuthContextProvider>
      <ListenerContextProvider>
        <MainNavigator />
      </ListenerContextProvider>
    </AuthContextProvider>
  );
}
