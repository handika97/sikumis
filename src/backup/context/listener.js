import React, {createContext, useReducer, useEffect} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {listenerReducer} from '../reducers/listener';

export const listenerContext = createContext();

const ListenerContextProvider = (props) => {
  const initialState = {
    status: false,
    date: '',
    fuzzy: '',
    now: '',
    Indikator: '',
    Kelembapan: '',
    Curah_Hujan: '',
    values: {},
    vanue: '',
  };

  const [list, dispatch_listener] = useReducer(listenerReducer, initialState);

  storeDataLang = async () => {
    try {
      await AsyncStorage.setItem('listener', JSON.stringify(list));
    } catch (e) {
      // saving error
    }
  };

  getDataLang = async () => {
    try {
      const value = await AsyncStorage.getItem('listener');
      if (value !== null) {
        // console.log(value)
        dispatch_listener({
          type: 'RESET_lANG',
          current_lang: JSON.parse(value),
        });
      }
    } catch (e) {
      // error reading value
    }
  };

  useEffect(() => {
    getDataLang();
  }, []);

  useEffect(() => {
    storeDataLang();
    // console.log("test",lang)
  }, [list]);

  return (
    <listenerContext.Provider value={{list, dispatch_listener}}>
      {props.children}
    </listenerContext.Provider>
  );
};

export default ListenerContextProvider;
