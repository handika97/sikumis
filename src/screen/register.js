import React, {useEffect, useContext, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  NativeModules,
  ImageBackground,
  TextInput,
  ActivityIndicator,
} from 'react-native';
import database from '@react-native-firebase/database';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ScrollView} from 'react-native-gesture-handler';
import {listenerContext} from '../context/listener';
import {authContext} from '../context/authSlice';
import authh from '@react-native-firebase/auth';

const Register = ({navigation}) => {
  const {auth, dispatch_auth} = useContext(authContext);
  const [Username, setUsername] = useState('');
  const [Password, setPassword] = useState('');
  const [data, setdata] = useState('');
  const [Visible, setVisible] = useState(false);

  console.log(auth);

  const regis = () => {
    if (Username && Password) {
      setVisible(true);

      authh()
        .createUserWithEmailAndPassword(Username, Password)
        .then(() => {
          dispatch_auth({
            type: 'LOGIN',
            name: Username.toUpperCase(),
          });
          setVisible(false);
        })
        .catch((error) => {
          setVisible(false);

          if (error.code === 'auth/email-already-in-use') {
            alert('That email address is already in use!');
          }

          if (error.code === 'auth/invalid-email') {
            alert('That email address is invalid!');
          }

          console.error(error);
        });
    } else {
      alert('Username atau Password Tidak Boleh Kosong');
    }
  };

  return (
    <View style={styles.container}>
      {!Visible ? (
        <>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <View style={{height: wp(12), width: wp(80)}}>
              <Image
                source={require('../asset/sikumis1.png')}
                style={{height: '100%', width: '100%'}}
              />
            </View>
            <View
              style={{
                height: 50,
                width: wp(80),
                marginTop: hp(3),
                borderWidth: 1,
                borderRadius: 30,
                borderColor: 'black',
                alignItems: 'center',
                padding: 5,
                backgroundColor: 'white',
                flexDirection: 'row',
              }}>
              <View
                style={{
                  height: 35,
                  width: 35,
                  position: 'absolute',
                  marginLeft: wp('2'),
                }}>
                <Image
                  source={require('../asset/user2.png')}
                  style={{height: '100%', width: '100%'}}
                />
              </View>
              <View style={{width: '100%', alignItems: 'center'}}>
                <TextInput
                  placeholder="Email"
                  value={Username}
                  onChangeText={setUsername}
                />
              </View>
            </View>
            <View
              style={{
                height: 50,
                width: wp(80),
                marginTop: hp(3),
                borderWidth: 1,
                borderRadius: 30,
                borderColor: 'black',
                alignItems: 'center',
                padding: 5,
                flexDirection: 'row',
                backgroundColor: 'white',
              }}>
              <View
                style={{
                  height: 35,
                  width: 35,
                  position: 'absolute',
                  marginLeft: wp('2'),
                }}>
                <Image
                  source={require('../asset/padlock2.png')}
                  style={{height: '100%', width: '100%'}}
                />
              </View>
              <View style={{width: '100%', alignItems: 'center'}}>
                <TextInput
                  placeholder="Password"
                  secureTextEntry
                  value={Password}
                  onChangeText={setPassword}
                />
              </View>
            </View>
            <TouchableOpacity
              onPress={() => regis()}
              style={{
                alignSelf: 'center',
                backgroundColor: '#d4d8d7',
                height: 43,
                width: wp(50),
                marginTop: hp(5),
                alignItems: 'center',
                justifyContent: 'center',
                borderWidth: 0.5,
                elevation: 5,
              }}>
              <Text
                style={{fontSize: hp(3.5), fontWight: 'bold', color: 'black'}}>
                Daftar
              </Text>
            </TouchableOpacity>
          </View>
        </>
      ) : (
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator size="large" color="black" />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#e8cecd',
    justifyContent: 'center',
  },
  view: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    backgroundColor: 'gray',
    padding: 10,
    margin: 10,
  },
  text: {
    fontSize: 20,
    color: 'white',
  },
});

export default Register;
