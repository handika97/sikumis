import React, {useEffect, useContext, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  NativeModules,
  ImageBackground,
  TextInput,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ScrollView} from 'react-native-gesture-handler';
import axios from 'axios';
import {authContext} from '../context/authSlice';
import {listenerContext} from '../context/listener';
import RNCalendarEvents from 'react-native-calendar-events';
import PushNotification from 'react-native-push-notification';

const intro = ({navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#e8cecd'}}>
      <View
        style={{
          paddingVertical: hp(4),
          paddingHorizontal: wp(2),
          justifyContent: 'center',
        }}>
        <View
          style={{
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
          }}>
          <View
            style={{
              height: hp(9),
              width: hp(9),
            }}>
            <Image
              source={require('../asset/logo.png')}
              style={{height: '100%', width: '100%'}}
            />
          </View>
        </View>
      </View>
      <View
        style={{
          alignSelf: 'center',
          flex: 1,
          justifyContent: 'center',
          marginTop: -hp(20),
          width: '100%',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontSize: hp(4),
          }}>
          SELAMAT DATANG
        </Text>
        <View
          style={{
            // elevation: 5,
            backgroundColor: '#e8cecd',
            width: '100%',
            alignItems: 'center',
            height: hp(6),
            justifyContent: 'center',
          }}>
          <TouchableOpacity onPress={() => navigation.navigate('introText')}>
            <Text
              style={{
                fontSize: hp(2),
              }}>
              Baca Yuk!
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <View
        style={{
          position: 'absolute',
          bottom: 0,
          backgroundColor: '#92c4e7',
          width: '100%',
          height: hp(8),
          justifyContent: 'center',
        }}>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPress={() => navigation.navigate('homeLogin')}>
          <Text style={{fontWeight: 'bold', fontSize: hp(2)}}>MULAI</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // flexDirection: 'column',
    // backgroundColor: 'white',
    // alignItems: 'center',
  },
});

export default intro;
