const initialState = {
  isLogin: false,
  name: '',
  data: [],
};

export const authReducer = (state, action) => {
  switch (action.type) {
    case 'LOGIN':
      return {
        ...state,
        isLogin: true,
        name: action.name,
      };
    case 'LOG_OFF':
      return {
        ...state,
        isLogin: false,
        name: '',
      };
    case 'FORGET_PASSWORD':
      return {
        ...state,
        data: action.data,
      };
    case 'Register':
      return {
        isLogin: true,
        name: action.name,
        data: action.data,
      };
    case 'RESET_LANG': {
      //   console.log("auth", action.current_lang);
      return action.current_lang;
    }
    default:
      return state;
  }
};
